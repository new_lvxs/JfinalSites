/**
 * 请勿将俱乐部专享资源复制给任何人，保护知识产权即是保护我们所在的行业，进而保护我们自己的利益
 * 即便是公司的同事，也请尊重 JFinal 作者的努力与付出，不要复制给同事
 * 
 * 如果你尚未加入俱乐部，请立即删除该项目，或者现在加入俱乐部：http://jfinal.com/club
 * 
 * 俱乐部将提供 jfinal-club 项目源码、直播视频、专用 QQ 群，以及作者在俱乐部定期的分享与答疑，
 * 价值远比仅仅拥有 jfinal club 项目源代码要大得多，俱乐部福利资源是不断增加的，以下是俱乐部
 * 新福利计划：
 * https://www.jfinal.com/club/1-2
 * 
 * JFinal 俱乐部是七年以来首次寻求外部资源的尝试，以便于创建更加高品质的产品与服务，为你带来
 * 更多、更大的价值
 * 
 * JFinal 项目的可持续性发展需要你的支持！！！
 */

package com.jfinal.club._admin.index;

import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;

/**
 * 首页业务
 */
public class IndexAdminService {

	public Ret getAccountProfile() {
		Ret ret = Ret.by("title", "账户总数");
		Integer total = Db.queryInt("select count(*) from account");
		ret.set("total", total);
		return ret;
	}

	public Ret getProjectProfile() {
		Ret ret = Ret.by("title", "项目总数");
		Integer total = Db.queryInt("select count(*) from project");
		ret.set("total", total);
		return ret;
	}

	public Ret getShareProfile() {
		Ret ret = Ret.by("title", "分享总数");
		Integer total = Db.queryInt("select count(*) from share");
		ret.set("total", total);
		return ret;
	}

	public Ret getFeedbackProfile() {
		Ret ret = Ret.by("title", "反馈总数");
		Integer total = Db.queryInt("select count(*) from feedback");
		ret.set("total", total);
		return ret;
	}

	public Ret getPermissionProfile() {
		Ret ret = Ret.by("title", "权限总数");
		Integer total = Db.queryInt("select count(*) from permission");
		ret.set("total", total);
		return ret;
	}
}