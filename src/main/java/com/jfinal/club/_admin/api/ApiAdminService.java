package com.jfinal.club._admin.api;

import com.jfinal.aop.Inject;
import com.jfinal.club.common.model.Api;
import com.jfinal.club.common.model.Api;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.club.common.model.FeedbackReply;
import com.jfinal.club.my.feedback.MyFeedbackService;
import com.jfinal.plugin.activerecord.Page;
import java.util.Date;
import java.util.List;

/**
 * Api 管理业务
 */
public class ApiAdminService {
	
	private Api dao = new Api().dao();

	/**
	 * 分页查询
	 */
	public Page<Api> paginate(int pageNum,int pageSize,String orderBy) {
		String sql = "from api";
		if(StrKit.notBlank(orderBy)) {
			sql += " order by " + orderBy;
		}
		else {
			sql += " order by id";
		}
		return dao.paginate(pageNum, pageSize, "select *", sql);
	}

	/**
	 * 新增保存API对象
	 * @param createUser 新增用户
	 * @param api API对象
	 * @return
	 */
	public Ret save(String createUser, Api api) {
		api.setCreateUser(createUser);
		api.setCreateTime(new Date());
		api.setUpdateUser(createUser);
		api.setUpdateTime(new Date());
		api.save();
		return Ret.ok("msg", "创建成功");
	}
	/**
	 * 修改Api对象
	 * @param id ID
	 * @return Api对象
	 */
	public Api edit(String id) {
		return dao.findById(id);
	}
	/**
	 * 更新Api对象
	 * @param updateUser 更新用户
	 * @param api Api对象
	 * @return
	 */
	public Ret update(String updateUser, Api api) {
		api.setUpdateUser(updateUser);
		api.setUpdateTime(new Date());
		api.update();
		return Ret.ok("msg", "修改成功");
	}
	
	/**
	 * 删除
	 * @param id ID
	 */
	public Ret delete(String id) {
		Api api = dao.findById(id);
		if (api != null) {
			api.delete();
			return Ret.ok("msg", "删除成功");
		} else {
			return Ret.fail("msg", "删除失败");
		}
	}
	/**
	 * 通过ID获取Api对象
	 * @param id ID
	 * @return 获取到的Api对象
	 */
	public Api getById(String id) {
		return dao.findById(id);
	}
}
